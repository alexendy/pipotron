#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Alex Coninx
    03/09/2019
""" 

import numpy as np
import re
import os
import time
import json

from pipotron.utils import readwords, reltoabspath


default_config="config_maconnique.json"




class Pipotron:
    def __init__(self, config_file=default_config):
        with open(reltoabspath(config_file),"r") as fd:
            config = json.load(fd)
        # Parse config :

        # Index
        if "index" in config:
            self.index_page = config["index"]
        else:
            self.index_page = "index.html"

        # Grammar
        with open(reltoabspath(config["grammar"]),"r") as fd:
            self.grammar = json.load(fd)

        # Terminals
        self.terminals = {}
        for (name, path) in config["terminals"].items():
            self.terminals[name] = readwords(reltoabspath(path))
        
        self.pattern = re.compile("\|([^\|]*)\|")


    def getSub(self, sub):
        if sub in self.terminals:
            return np.random.choice(self.terminals[sub])
        elif sub in self.grammar:
            return np.random.choice(self.grammar[sub][0], p=self.grammar[sub][1])
        else:
            print("Error: unknown element %s" % sub)
            sys.exit(1)

    def develop(self, string):
        expr = str(string)
        matches = self.pattern.findall(expr)
        while(len(matches) > 0):
            subs = list()
            for m in matches:
                subs.append(self.getSub(m))
            for (i,m) in enumerate(matches):
                expr = re.sub("\|%s\|" % m, subs[i], expr, count=1)
            #print(expr+"\n")
            matches = self.pattern.findall(expr)
        return expr
    
    def get_phrase(self):
        return(self.develop("|ROOT|"))
    
    def get_phrases(self,n):
        return(list([self.get_phrase() for i in range(n)]))



if(__name__ == '__main__'):
    p = Pipotron()
    n = 1
    if(len(sys.argv) > 1):
        n = int(sys.argv[1])
    for i in range(n):
        print(p.get_phrase())
